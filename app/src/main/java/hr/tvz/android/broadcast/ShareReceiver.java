package hr.tvz.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RatingBar;
import android.widget.Toast;

public class ShareReceiver extends BroadcastReceiver {

    private RatingBar rating;

    public ShareReceiver( RatingBar rating) {
        this.rating = rating;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Rating shared!", Toast.LENGTH_LONG).show();

        if(intent.getFloatExtra("rating", 1) != 0.0) {
            float userRating = intent.getFloatExtra("rating", 1);
            rating.setRating(userRating);
        }

    }
}
