package hr.tvz.android.broadcast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.RatingBar;


public class MainActivity extends AppCompatActivity {
    ShareReceiver shareReceiver;
    IntentFilter intentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RatingBar rating=findViewById(R.id.ratingBar);
        shareReceiver=new ShareReceiver(rating);

        intentFilter=new IntentFilter("hr.android.broadcast.share");
        registerReceiver(shareReceiver, intentFilter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(shareReceiver);
    }

}